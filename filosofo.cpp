#include<iostream>
#include<mutex>
#include<thread>
#include<ctime>
#include<chrono>

using namespace std;

const int num_Phil=5;//numnero de filosofos
thread philosphers[num_Phil];// Fisolofoso como threads
mutex mtx[num_Phil];//tenedores
mutex cout_mutex;
int ate[num_Phil];
//int intrupt=50;
int intrupt[num_Phil];//platos con cierta cantidad de comida


void stomach(){

    for(int i=0;i<num_Phil;i++){
        ate[i]=100;
        intrupt[i]=50;//Se le da un valor que solo se puede comer hasta 50
    }
}

void print(string str){
	cout_mutex.lock();
	cout<<str<<endl;
	cout_mutex.unlock();
}

void think(int id){//El filósofo "id" está pensando
    int thinking=1000+rand()%(2000-1000);//agregar un tiempo aleatoria para que piense el filosofo
  this_thread::sleep_for(chrono::nanoseconds(thinking));
  print("Philosopher " + to_string(id) + " is thiking.");
  ate[id]-=12;//Estomago disminuye en 12;
  if(ate[id]<=100 && thinking>= 1050){
      print("Philosopher "+ to_string(id) + " ha muerto de hambre"); //morira de hambre si cump con el if
  }
}

bool eat(int id, int left, int right){ //Función, el filósofo "id" está comiendo (intentando)
	while(1) if(mtx[left].try_lock()){ //  El filósofo "Id" tratando de levantar el tenedor izquierdo. (continúa hasta el éxito ")
	if(ate[id]>100) return true;//Si el filosofo come de mas, debe dejar de comer.
    print("Philosopher "+ to_string(id) + " got to fork"+ to_string(left));
	// Él tiene el tenedor ... Así que imprímelo
	
	if(mtx[right].try_lock()){// El "id" del filósofo intenta tomar el tenedor de la derecha.
        int eating=1000+rand()%(2000-1000);// Agrega un tiempo aleatorio mientras el filosofo come
        this_thread::sleep_for(chrono::nanoseconds(eating));
		print("Philosopher "+ to_string(id) +" got the ford "+ to_string(right) +
		"\nPhilosopher"+ to_string(id)+" eats.");// print
        ate[id]+=12;//El estomago aumenta en 12
		return true;
	}else{
		mtx[left].unlock();
		think(id);
		// Suelta el tenedor de la  izquierda ya que filosofo  no puede coger la derecha
        // Añadido para que podamos evitar el estancamiento y el hambre.
	}
	}
	return false;
}


void putDownForks(int left, int right){
	mtx[left].unlock();//baja el tenedor izquierdo
	mtx[right].unlock();//baja el tenedor derecho
}

void dinner_started(int philID){
	
	// Aquí se determinan los índices de los tenedores de izquierda y derecha
    // hay un método implementado aquí, tomando el min numerado
    // Tenedor primero
	
	int lIndex= min(philID,(philID+1)%(num_Phil));
	int rIndex= max(philID,(philID+1)%(num_Phil));


	while(intrupt[philID]-- > 0){  // Intrupt es el var, que uso para dar un límite a la hora de la cena
		if(eat(philID,lIndex,rIndex)){  // El filósofo "phillId" está tratando de comer, tenedores, lIndex & rIndex
			putDownForks(lIndex,rIndex); // Deja las tenedores
			//ate[philID]++;// Este filósofo: "phillId" comió una vez más 
			this_thread::sleep_for(chrono::milliseconds(600));// no puede adquirir la comida de inmediato
		}
	}
}

void dine(){

    
    // threads creados. Cada thread es un filósofo. A todo filósofo se le llama función "dinned_started"
	for(int i=0;i<num_Phil;++i){
		philosphers[i]=thread(dinner_started, i);
	}
    // threads unidos
	for(int i=0; i<num_Phil;++i){
		philosphers[i].join();
	}
}


int main(){
	stomach();
	dine();
	
	for(int i=0; i<num_Phil;i++){
		cout<<i<<" = "<<ate[i] <<endl;
	}
}
