#include<iostream>
#include<mutex>
#include<thread>
#include<ctime>
#include<chrono>

using namespace std;

const int num_Phil=5;//numnero de filosofos
thread philosphers[num_Phil];// Fisolofoso como threads
mutex mtx[num_Phil];//tenedores
mutex cout_mutex;
int ate[num_Phil]={0};
int intrupt=50;

void print(string str){
	cout_mutex.lock();
	cout<<str<<endl;
	cout_mutex.unlock();
}

void think(int id){//El filósofo "id" está pensando
  this_thread::sleep_for(chrono::milliseconds(600));
  print("Philosopher " + to_string(id) + " is thiking.");
}

bool eat(int id, int left, int right){ //Función, el filósofo "id" está comiendo (intentando)
	while(1) if(mtx[left].try_lock()){ //  El filósofo "Id" tratando de levantar el tenedor izquierdo. (continúa hasta el éxito ")
	print("Philosopher "+ to_string(id) + " got to fork"+ to_string(left));
	// Él tiene el tenedor ... Así que imprímelo
	
	if(mtx[right].try_lock()){
        // El "id" del filósofo intenta tomar el tenedor de la derecha.
		print("Philosopher "+ to_string(id) +" got the ford "+ to_string(right) +
		"\nPhilosopher"+ to_string(id)+" eats.");// print
    
		return true;
	}else{
		mtx[left].unlock();
		think(id);
		// Suelta el tenedor de la  izquierda ya que filosofo  no puede coger la derecha
        // Añadido para que podamos evitar el estancamiento y el hambre.
	}
	}
	return false;
}


void putDownForks(int left, int right){
	mtx[left].unlock();//baja el tenedor izquierdo
	mtx[right].unlock();//baja el tenedor derecho
}

void dinner_started(int philID){
	
	// Aquí se determinan los índices de los tenedores de izquierda y derecha
    // hay un método implementado aquí, tomando el min numerado
    // Tenedor primero
	
	int lIndex= min(philID,(philID+1)%(num_Phil));
	int rIndex= max(philID,(philID+1)%(num_Phil));


	while(intrupt-- > 0){  // Intrupt es el var, que uso para dar un límite a la hora de la cena
		if(eat(philID,lIndex,rIndex)){  // El filósofo "phillId" está tratando de comer, tenedores, lIndex & rIndex
			putDownForks(lIndex,rIndex); // Deja las tenedores
			//ate[philID]++;// Este filósofo: "phillId" comió una vez más 
			this_thread::sleep_for(chrono::milliseconds(600));// no puede adquirir la comida de inmediato
		}
	}
}

void dine(){

    
    // threads creados. Cada thread es un filósofo. A todo filósofo se le llama función "dinned_started"
	for(int i=0;i<num_Phil;++i){
		philosphers[i]=thread(dinner_started, i);
	}
    // threads unidos
	for(int i=0; i<num_Phil;++i){
		philosphers[i].join();
	}
}


int main(){
	
	dine();
	
	for(int i=0; i<num_Phil;i++){
		cout<<i<<" = "<<ate[i] <<endl;
	}
}
